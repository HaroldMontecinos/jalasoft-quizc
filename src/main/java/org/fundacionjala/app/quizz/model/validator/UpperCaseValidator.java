package org.fundacionjala.app.quizz.model.validator;

import java.util.List;

public class UpperCaseValidator implements Validator {

    private static final String ERROR_MESSAGE = "The value must be uppercase ";

    @Override
    public void validate(String value, String conditionValueString, List<String> errors) {
        System.out.println("llego a uper");
        try {
            String conditionValue = conditionValueString;

            if (value.toUpperCase()!= conditionValue) {
                errors.add(ERROR_MESSAGE + conditionValue);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
